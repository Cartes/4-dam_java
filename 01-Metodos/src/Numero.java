
public class Numero {
	
	//atributos
	private int valor;

	//Constructor
	public Numero(int v) {
		super(); //Para llamar a la clase base
		this.valor = v;
	}
	
	public int Sumar (int v) {
		
		int resultado;
		resultado = valor + v;
		return resultado;
	}
	
	
	public int DevolverValor () {
		
		return valor;
	}
	
	public void FijarValor(int nuevo) {
		valor = nuevo;
	}
	
	//Metodo que esta dentro y no utiliza nada de la clase
	public static int Sumar(int n1, int n2) {
		
		int resultado;
		
		resultado = n1 + n2;
		
		return resultado;
		
	}
	
	//Los metodos que son static no pueden acceder a las variables de su misma clase 
	//Metodos que son de la clase pero que no pueden acceder a la clase <-- static
	
	
}
