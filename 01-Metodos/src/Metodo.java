
public class Metodo {
	
	public static int Sumar(int n1, int n2) { // Los metodos que son staticos, no necesitan el objeto de la clase
		
		int resultado; 
		resultado = n1 + n2;
		return resultado;
	}
	
	public static int sumarNumero(int n1, Numero n) { // Valor primitivo y valor de clase
		
		int resultado;
		resultado = n1 + n.DevolverValor();
		n1 = 0;
		n.FijarValor(5);
		return resultado;
	}

	public static void main(String[] args) {
		
//		int numero1 = 23,
//			numero2 = 41,
//			numero3 = 11,
//		res;
//		
//		res = Sumar(numero1, numero2);
//		
//		System.out.println("El resultado es "+ res);
//		res = Sumar(numero1,numero3);
//		System.out.println("El resultado es "+ res);

		Numero miNumero = new Numero(21),
			   otroNumero = new Numero(33);
		

		System.out.println("El primero es " + miNumero.DevolverValor());
		System.out.println("El primero es " + otroNumero.DevolverValor());
		
		// resultado = resultado + 1;
		
		int resultado;
		
		resultado = miNumero.Sumar(1);
		System.out.println("La suma es:  " + resultado);
		
		//Llamada a m�todo static
		
		resultado = Numero.Sumar(21, 33);
		System.out.println("La suma es: " + resultado);
		
		//Llamada con par�metros de clase
		
		Numero esteNumero = new Numero(4);
		int numero1 = 3;
		//Numero 1 = 3 y esteNumero = 4
		resultado = sumarNumero(numero1, esteNumero);
		System.out.println("numero1 vale:  "+ numero1);
		System.out.println("esteNumero vale: "+ esteNumero.DevolverValor());
		System.out.println("La suma es: "+ resultado);
		
		
		
		

	}

}
