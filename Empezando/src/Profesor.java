
public class Profesor extends Administracion{
	
	int NAsignaturas;
	char tutor;
	
	//Metodo Constructor
	public Profesor(String dni, String nombre, String apellido, double salario, int nAsignaturas, char tutor) {
		super(dni, nombre, apellido, salario);
		NAsignaturas = nAsignaturas;
		this.tutor = tutor;
	}

	//Get y Set
	public int getNAsignaturas() {
		return NAsignaturas;
	}

	public void setNAsignaturas(int nAsignaturas) {
		NAsignaturas = nAsignaturas;
	}

	public char getTutor() {
		return tutor;
	}

	public void setTutor(char tutor) {
		this.tutor = tutor;
	}

	@Override
	public String toString() {
		return "Profesor [dni=" + dni + ", nombre=" + nombre + ", apellido=" + apellido + ", salario=" + salario
				+ ", NAsignaturas=" + NAsignaturas + ", tutor=" + tutor + "]";
	}
	
	
	
	
	
	


}
