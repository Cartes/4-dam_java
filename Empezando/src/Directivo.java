
public class Directivo extends Administracion{
	
	char salesiano;
	char horario;
	
	//Metodo constructor
	public Directivo(String dni, String nombre, String apellido, double salario, char salesiano, char horario) {
		super(dni, nombre, apellido, salario);
		this.salesiano = salesiano;
		this.horario = horario;
	}

	//Metodos Get y SET
	public char getSalesiano() {
		return salesiano;
	}

	public void setSalesiano(char salesiano) {
		this.salesiano = salesiano;
	}

	public char getHorario() {
		return horario;
	}

	public void setHorario(char horario) {
		this.horario = horario;
	}

	//ToString
	@Override
	public String toString() {
		return "Directivo [dni=" + dni + ", nombre=" + nombre + ", apellido=" + apellido + ", salario=" + salario
				+ ", salesiano=" + salesiano + ", horario=" + horario + "]";
	}


	
	
	
	
	

}
