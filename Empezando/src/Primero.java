
public class Primero {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int primero = 0;
		String nombre = "Alejandro Cartes";
		double altura = 1.72;
		char sexo = 'M';
		boolean madrid = true;
		
		int [] pares = {8,3,2,5,9,1,3,4,6,2};
		int sumando = 0;
		
		System.out.println("El numero es: "+primero);
		System.out.println("Tu nombre es: "+nombre);
		System.out.println("Tu altura es: "+altura);
		System.out.println("El sexo es: "+sexo);
		System.out.println("�Eres de madrid? "+madrid);
		
		//Formula para recorrer un array de enteros y sacar los numeros pares
		for (int i=0; i<pares.length; i++) {
			if(pares[i] % 2 == 0) {
			sumando += pares[i];
			System.out.println("Los numeros del array pares: "+pares[i]);
			}
			else {
				System.out.println("No vale");
			}
		
		}
		
		int [][] matriz1 = {{1,5,4}, {5,6,2}};
		int [][] matriz2 = {{4,5,3}, {2,3,1}};
		int [][] total = new int[2][3];
		
		//Formula para sumar matrices
		for (int f=0; f < 2; f++) {
			System.out.println();
			for(int c=0; c < 3; c++) {
				total[f][c] = matriz1[f][c] + matriz2[f][c];
				System.out.print("|"+total[f][c]);
			}
		}
		
		
		// Calcular el factorial de un numero entero positivo a traves de la recursividad
		
		System.out.println();
		System.out.println(factorial(5));

	}
	
	public static int factorial(int positivo) {
		//condicion de parada
		if (positivo == 1)
			return 1;
		else
			return positivo * factorial(positivo-1);
	}

}
