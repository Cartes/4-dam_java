
public class modulo {
	
	String nombre;
	int Nhoras;
	String profesor;
	char convalidable;
	
	//Metodo Constructor
	public modulo(String nombre, int nhoras, String profesor, char convalidable) {
		super();
		this.nombre = nombre;
		Nhoras = nhoras;
		this.profesor = profesor;
		this.convalidable = convalidable;
	}

	
	//GET & SET
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNhoras() {
		return Nhoras;
	}

	public void setNhoras(int nhoras) {
		Nhoras = nhoras;
	}

	public String getProfesor() {
		return profesor;
	}

	public void setProfesor(String profesor) {
		this.profesor = profesor;
	}

	public char getConvalidable() {
		return convalidable;
	}

	public void setConvalidable(char convalidable) {
		this.convalidable = convalidable;
	}

	@Override
	public String toString() {
		return "modulo [nombre=" + nombre + ", Nhoras=" + Nhoras + ", profesor=" + profesor + ", convalidable="
				+ convalidable + "]";
	}
	
	
}
