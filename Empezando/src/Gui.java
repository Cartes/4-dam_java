import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.DropMode;
import javax.swing.JPasswordField;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JRadioButton;
import javax.swing.JScrollBar;
import javax.swing.JProgressBar;
import javax.swing.JEditorPane;
import javax.swing.AbstractListModel;

public class Gui extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;
	private JButton btnAceptar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui frame = new Gui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Gui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 766, 444);
		contentPane = new JPanel();

		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(248, 117, 65, 14);
		contentPane.add(lblUsuario);
		
		JLabel lblContrasea = new JLabel("Contrase\u00F1a");
		lblContrasea.setBounds(248, 153, 65, 14);
		contentPane.add(lblContrasea);
		
		btnAceptar = new JButton("Salir");

		btnAceptar.setBounds(562, 331, 89, 23);
		contentPane.add(btnAceptar);
		
		textField = new JTextField();
		textField.setBounds(323, 114, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JCheckBox chckbxAceptasLasCondiciones = new JCheckBox("Aceptas las condiciones");
		chckbxAceptasLasCondiciones.setBounds(248, 186, 164, 23);
		contentPane.add(chckbxAceptasLasCondiciones);
		
		passwordField = new JPasswordField();
		passwordField.setEchoChar('�');
		passwordField.setBounds(323, 150, 86, 20);
		contentPane.add(passwordField);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"MADRID", "BARCELONA", "VALENCIA", "ALACANT", "GALICIA"}));
		comboBox.setBounds(70, 249, 75, 20);
		contentPane.add(comboBox);
		
		JList list = new JList();
		list.setBounds(284, 329, 65, -47);
		contentPane.add(list);
		
		JRadioButton boton = new JRadioButton("\u00BFEres de madrid?");
		boton.setBounds(248, 248, 133, 23);
		contentPane.add(boton);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(206, 293, 133, 80);
		contentPane.add(scrollPane);
		
		JList list_1 = new JList();
		scrollPane.setViewportView(list_1);
		list_1.setModel(new AbstractListModel() {
			String[] values = new String[] {"A", "A", "A", "A", "A", "A", "A", "A"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			int respuesta = JOptionPane.showConfirmDialog(null, 
					"�Realmente desea salir?", 
					"Confirmar salida", 
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if(respuesta == 0)
				System.exit(0);
			}
		});
		
	}
}
